const Navbar = () => {
    return (
      <nav className="navbar">
        <h1>CCS React Application</h1>
        <div className="links">
          <a href="/"
          //style 1
          style={{ 
          color: 'white', 
          backgroundColor: '#b287a3',
          borderRadius: '8px' 
        }}>Blog</a>

          <a href="/contact"
          //style 2
          style={{ 
          color: 'white', 
          backgroundColor: '#f7aef8',
          borderRadius: '8px' 
        }}>Contact</a>

        </div>
      </nav>
    );
  }
   
  export default Navbar;