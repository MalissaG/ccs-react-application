const Contact = () => {

    const handleClick = (status) => {
      console.log('I am '+ status);
    }
  
    const handleClickAgain = (number) => {
      console.log('My phone number is ' + number);
    }
  
    const handleClickAgain1 = (DOB) => {
        console.log('I was born on ' + DOB);
    }

    return (
      <div className="contact">
        <h2>Contact</h2>

        <button onClick={handleClick('Married')}
        //style 1
          style={{ 
          color: 'white', 
          backgroundColor: '#32533D',
          borderRadius: '8px',
          padding: '12px',
          margin: '16px'
        }}>Status</button>


        <button onClick={() => handleClickAgain('6499883')} 
        //style 2
          style={{ 
          color: 'white', 
          backgroundColor: '#ED9390',
          borderRadius: '8px',
          padding: '12px',
          margin: '16px'
         }}>Phone Number</button>


        <button onClick={() => handleClickAgain1('April 6th, 2001')} 
        //style 2
          style={{ 
          color: 'white', 
          backgroundColor: '#37515F',
          borderRadius: '8px',
          padding: '12px',
          margin: '16px'
         }}>Date of <Birth></Birth></button>

      </div>
    );
  }
  

  export default Contact;