const Blog = () => {

    const handleClick = () => {
      console.log('Hello you');
    }
  
    const handleClickAgain = (name) => {
      console.log('Hello ' + name);
    }
  
    return (
      <div className="blog">
        <h2>Blog</h2>
        <button onClick={handleClick}
        //style 1
          style={{ 
          color: 'white', 
          backgroundColor: '#4E3822',
          borderRadius: '8px',
          padding: '12px',
          margin: '16px'
         
        
          
        }}>Hello</button>


        <button onClick={() => handleClickAgain('Malissa')} 
        //style 2
          style={{ 
          color: 'white', 
          backgroundColor: '#2F1B25',
          borderRadius: '8px',
          padding: '12px',
          margin: '16px'
         

          }}>Name</button>

      </div>
    );
  }
  

  export default Blog;